//
//  DetalheViewController.h
//  PhotoFamily
//
//  Created by EDIT Guest on 29/04/13.
//  Copyright (c) 2013 EDIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WorkView.h"

@interface PFDetailVC : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *imgViewDetail;
@property(strong, nonatomic)WorkView * theWorkView;
@end
