//
//  HomeViewController.m
//  PhotoFamily
//
//  Created by EDIT Guest on 29/04/13.
//  Copyright (c) 2013 EDIT. All rights reserved.
//

#import "PFHomeVC.h"
#import <Parse/Parse.h>
#import "EDArtworkManager.h"
#import "EDArtwork.h"
#import "WorkView.h"
#import "PFDetailVC.h"

@interface PFHomeVC (){

    WorkView * workViewDetail;

}

@property(strong, nonatomic)NSMutableArray * arrayOfWorkViews;
@end

@implementation PFHomeVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _arrayOfWorkViews = [NSMutableArray new];
    
    UIScrollView * scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(30, 20, 940, 510)];
    scrollView.backgroundColor = [UIColor whiteColor];
    self.view = scrollView;


    
    [[EDArtworkManager sharedManager] getAllArtworksForUserId:@"User1" withCompletionBlock:^(NSError *error, NSArray *artworks) {
        if(error){
        
        }
        else {
            WorkView * myWork;
            for (unsigned i = 0; i < [artworks count]; i++) {
                
                EDArtwork *artwork = [artworks objectAtIndex:i];
                
                if ([_arrayOfWorkViews count] > 0){
                    
                    WorkView * lastWorkView = [_arrayOfWorkViews objectAtIndex: i - 1];
                    
                    myWork = [[WorkView alloc] initWithFrame:CGRectMake(30, lastWorkView.frame.origin.y + lastWorkView.frame.size.height + 20, 940, 310) withWorkImage:artwork.imageURL andAuthorImage:artwork.authorURL andText:artwork.text];
                    myWork.delegate = self;
                    
                    //NSLog(@"H:%f   W:%f   Y:%f   X:%f",myWork.frame.size.height, myWork.frame.size.width, myWork.frame.origin.y, myWork.frame.origin.x );
                }else{
                    myWork = [[WorkView alloc] initWithFrame:CGRectMake(30, 20, 940, 310) withWorkImage:artwork.imageURL andAuthorImage:artwork.authorURL andText:artwork.text];
                    //NSLog(@"H:%f   W:%f   Y:%f   X:%f",myWork.frame.size.height, myWork.frame.size.width, myWork.frame.origin.y, myWork.frame.origin.x );
                    myWork.delegate = self;
                }
                 
                [_arrayOfWorkViews addObject:myWork];
              
                [self.view addSubview:myWork];
                
                //NSLog(@"<%@ : %@> ", artwork.authorURL, artwork.text);
                
            }
            
            float sdf = myWork.frame.size.height * [_arrayOfWorkViews count] + 20 * [_arrayOfWorkViews count];
            
            scrollView.contentSize = CGSizeMake(self.view.frame.size.width,sdf);
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)logoutButtonTouchHandler:(id)sender {
    // Logout user, this automatically clears the cache
    [PFUser logOut];
    
    // Return to login view controller
    [self.navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark - WORKVIEW DELEGATE METHODS

-(void)likeBtnPressed:(WorkView *)workLiked{

    NSLog(@"%@", workLiked.detailTextView.text);

}


-(void)viewWorkDetailBtnPressed:(WorkView *)imageToViewDetail{

    NSLog(@"Image in parameter");
    workViewDetail = imageToViewDetail;
    [self performSegueWithIdentifier:@"goToWorkDetail" sender:self];


}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"goToWorkDetail"]) {
        
        // Get destination view
        PFDetailVC * vc = [segue destinationViewController];
        vc.theWorkView = workViewDetail;
    }
    
}

@end
