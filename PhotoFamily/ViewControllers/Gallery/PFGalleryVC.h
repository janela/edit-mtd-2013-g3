//
//  PFGalleryVC.h
//  PhotoFamily
//
//  Created by EDIT Guest on 19/06/13.
//  Copyright (c) 2013 EDIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WorkView.h"

@interface PFGalleryVC : UICollectionViewController <WorkViewDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@end
