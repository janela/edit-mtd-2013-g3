//
//  PFGalleryVC.m
//  PhotoFamily
//
//  Created by EDIT Guest on 19/06/13.
//  Copyright (c) 2013 EDIT. All rights reserved.
//

#import "PFGalleryVC.h"
#import "EDArtworkManager.h"
#import "EDArtwork.h"
#import "WorkView.h"
#import "PFDetailVC.h"

@interface PFGalleryVC ()  {
    NSMutableArray *recipeImages;
    WorkView * workViewDetail;
}

@end

@implementation PFGalleryVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
   
    recipeImages = [[NSMutableArray alloc] init];
    
    [[EDArtworkManager sharedManager] getAllArtworksForUserId:@"User1" withCompletionBlock:^(NSError *error, NSArray *artworks) {
        if(error){
            
        }
        else {
            for (EDArtwork *artwork in artworks) {
                NSLog(@"%@", artwork.imageURL);
                [recipeImages addObject:artwork.imageURL];
            }
            
            [self.collectionView reloadData];
        }
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSLog(@"%@", recipeImages);
    return recipeImages.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    
    UIImageView *recipeImageView = (UIImageView *)[cell viewWithTag:100];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage * image  = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[recipeImages objectAtIndex:indexPath.row]]]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            recipeImageView.image = image;
            
        });
        
    });

    return cell;
}


-(void)likeBtnPressed:(WorkView *)workLiked{
    
    NSLog(@"%@", workLiked.detailTextView.text);
    
}

-(void)viewWorkDetailBtnPressed:(WorkView *)imageToViewDetail{
    
    NSLog(@"Image in parameter");
    workViewDetail = imageToViewDetail;
    [self performSegueWithIdentifier:@"showGalleryImage" sender:self];
    
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"showGalleryImage"]) {
        
        // Get destination view
        PFDetailVC * vc = [segue destinationViewController];
        vc.theWorkView = workViewDetail;
    }
    
}

@end
