//
//  FamilyViewController.h
//  PhotoFamily
//
//  Created by EDIT Guest on 29/04/13.
//  Copyright (c) 2013 EDIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface PFFamilyVC : UIViewController

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end
