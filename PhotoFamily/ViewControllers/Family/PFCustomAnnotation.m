//
//  PFCustomAnnotation.m
//  PhotoFamily
//
//  Created by EDIT Guest on 19/06/13.
//  Copyright (c) 2013 EDIT. All rights reserved.
//

#import "PFCustomAnnotation.h"

@implementation PFCustomAnnotation

@synthesize title, subtitle, coordinate;

- (id)initWithTitle:(NSString *)aTitle subtitle:(NSString*)aSubtitle andCoordinate:(CLLocationCoordinate2D)coord
{
	self = [super init];
	title = aTitle;
    subtitle = aSubtitle;
	coordinate = coord;
	return self;
}

@end
