//
//  FamilyViewController.m
//  PhotoFamily
//
//  Created by EDIT Guest on 29/04/13.
//  Copyright (c) 2013 EDIT. All rights reserved.
//

#import "PFFamilyVC.h"
#import "PFCustomAnnotation.h"
#import "EDArtworkManager.h"
#import "EDArtwork.h"
#import "WorkView.h"

#define METERS_PER_MILE 1609.344

@interface PFFamilyVC ()

@end

@implementation PFFamilyVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    

    CLLocationCoordinate2D initialLocation;
    initialLocation.latitude = 38.710345;
    initialLocation.longitude= -9.134853;
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(initialLocation, 10000, 10000);
    
    [self.mapView setRegion:region animated:YES];
    
    
    
   	[[EDArtworkManager sharedManager] getAllArtworksForUserId:@"User1" withCompletionBlock:^(NSError *error, NSArray *artworks) {
        if(error){
        
        }
        else {
            for (EDArtwork *artwork in artworks) {
                
                float latFloat = [artwork.latitude floatValue];
                float lonFloat = [artwork.longitude floatValue];
                
                CGFloat newLat = latFloat;
                CGFloat newLon = lonFloat;
                
                CLLocationCoordinate2D newCoord = {newLat, newLon};
                
                PFCustomAnnotation *annotation = [[PFCustomAnnotation alloc]initWithTitle: artwork.title subtitle: artwork.author andCoordinate:newCoord];
                
                [self.mapView addAnnotation:annotation];
            }
        }
    }];
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
