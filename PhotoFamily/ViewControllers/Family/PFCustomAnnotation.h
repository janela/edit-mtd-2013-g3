//
//  PFCustomAnnotation.h
//  PhotoFamily
//
//  Created by EDIT Guest on 19/06/13.
//  Copyright (c) 2013 EDIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface PFCustomAnnotation : NSObject<MKAnnotation> {
    
    NSString *title;
    NSString *subtitle;
    CLLocationCoordinate2D coordinate;
    
}

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

- (id)initWithTitle:(NSString *)aTitle subtitle:(NSString*)aSubtitle andCoordinate:(CLLocationCoordinate2D)coord;

@end
