//
//  PFLoginController.h
//  PhotoFamily
//
//  Created by EDIT Guest on 03/06/13.
//  Copyright (c) 2013 EDIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PFLoginVC : UIViewController


@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;

- (IBAction)loginBtnPressed:(id)sender;

@end
