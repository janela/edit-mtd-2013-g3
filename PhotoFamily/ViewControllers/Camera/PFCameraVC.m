//
//  CameraViewController.m
//  PhotoFamily
//
//  Created by EDIT Guest on 29/04/13.
//  Copyright (c) 2013 EDIT. All rights reserved.
//

#import "PFCameraVC.h"

#import "UIImage+Filters.h"

@interface PFCameraVC ()

@property (nonatomic, strong) UIImage *originalImage;

@end

@implementation PFCameraVC
@synthesize imageView;
@synthesize originalImage = _originalImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
        _newMedia = YES;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
	
}

- (IBAction)resetFilterBt:(id)sender {
    [self.imageView setImage:self.originalImage];
}

- (IBAction)applyFilterBt:(UIButton *)sender{
    
    NSString *buttonTitle = sender.titleLabel.text;
    
    if ([buttonTitle isEqualToString:@"Saturation"]) {
        self.imageView.image = [self.imageView.image saturateImage:1.7 withContrast:1];
    }
    
    if ([buttonTitle isEqualToString:@"B&W"]) {
        self.imageView.image = [self.imageView.image saturateImage:0 withContrast:1.05];
    }
    
    if ([buttonTitle isEqualToString:@"Vignette"]) {
        self.imageView.image = [self.imageView.image vignetteWithRadius:0 andIntensity:18];
    }
    
}



#pragma mark -
#pragma mark UIImagePickerControllerDelegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage])
    {
        UIImage *image = info[UIImagePickerControllerOriginalImage];
        
        imageView.image = image;
        if (_newMedia)
            UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:finishedSavingWithError:contextInfo:), nil);
        
        
        self.originalImage = self.imageView.image;
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"No Video Allowed" message:@"Video is not allowed in this app" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [message show];
    }
}

-(void)image:(UIImage *) image finishedSavingWithError:(NSError *) error contextInfo:(void *) contextInfo
{
    if(error)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Save failed" message:@"Failed to save image" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
