//
//  UIImage+Filters.m
//  PhotoFilters
//
//  Created by Roberto Breve on 6/2/12.
//  Copyright (c) 2012 Icoms. All rights reserved.
//

#import "UIImage+Filters.h"

@implementation UIImage (Filters)

- (UIImage*)saturateImage:(float)saturationAmount withContrast:(float)contrastAmount{
    UIImage *sourceImage = self;
    
    CIContext *context = [CIContext contextWithOptions:nil];
    
    CIFilter *filter= [CIFilter filterWithName:@"CIColorControls"];
    
    CIImage *inputImage = [[CIImage alloc] initWithImage:sourceImage];
    
    [filter setValue:inputImage forKey:@"inputImage"];
    
    [filter setValue:[NSNumber numberWithFloat:saturationAmount] forKey:@"inputSaturation"];
    [filter setValue:[NSNumber numberWithFloat:contrastAmount] forKey:@"inputContrast"];
    
    
    return [self imageFromContext:context withFilter:filter];
    
}


- (UIImage*)vignetteWithRadius:(float)inputRadius andIntensity:(float)inputIntensity{
 

     CIContext *context = [CIContext contextWithOptions:nil];
     
     CIFilter *filter= [CIFilter filterWithName:@"CIVignette"];
     
     CIImage *inputImage = [[CIImage alloc] initWithImage:self];
     
     [filter setValue:inputImage forKey:@"inputImage"];
     
     [filter setValue:[NSNumber numberWithFloat:inputIntensity] forKey:@"inputIntensity"];
     [filter setValue:[NSNumber numberWithFloat:inputRadius] forKey:@"inputRadius"];
     
     return [self imageFromContext:context withFilter:filter];
}




- (UIImage*)imageFromContext:(CIContext*)context withFilter:(CIFilter*)filter
{
    
    CGImageRef imageRef = [context createCGImage:[filter outputImage] fromRect:filter.outputImage.extent];
    UIImage *image = [UIImage imageWithCGImage:imageRef scale:self.scale orientation:self.imageOrientation];
    CGImageRelease(imageRef);
    return image;
    
}





@end
