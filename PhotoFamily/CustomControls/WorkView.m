//
//  WorkView.m
//  PhotoFamily
//
//  Created by EDIT Guest on 12/06/13.
//  Copyright (c) 2013 EDIT. All rights reserved.
//

#import "WorkView.h"

@implementation WorkView

- (id)initWithFrame:(CGRect)frame withWorkImage:(NSString *)workImage andAuthorImage:(NSString *)authorImage andText:(NSString *)detailText
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.frame = CGRectMake(frame.origin.x, frame.origin.y, 940, 310);
        
        _workImgView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, 400, 220)];
        _workImgView.contentMode = UIViewContentModeScaleAspectFill;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            UIImage * image  = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:workImage]]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _workImgView.image = image;
                
            });
            
        });
         
        
        _authorImgView = [[UIImageView alloc] initWithFrame:CGRectMake(822, 19, 100, 100)];
        _authorImgView.contentMode = UIViewContentModeScaleAspectFill;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            UIImage * image  = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:authorImage]]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _authorImgView.image = image;
                
            });
            
        });
        
        _detailTextView = [[UITextView alloc] initWithFrame:CGRectMake(430, 110, 290, 130)];
        _detailTextView.editable = NO;
        _detailTextView.text = detailText;
        
        _viewWorkDetailBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _viewWorkDetailBtn.frame = CGRectMake(20, 250, 60, 44);
        [_viewWorkDetailBtn setTitle:@"Detail" forState:UIControlStateNormal];
        [_viewWorkDetailBtn addTarget:self action:@selector(buttonDetailClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        _likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _likeBtn.frame = CGRectMake(365, 250, 55, 44);
        [_likeBtn setTitle:@"like" forState:UIControlStateNormal];
        [_likeBtn addTarget:self action:@selector(buttonLikeClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:_workImgView];
        [self addSubview:_authorImgView];
        [self addSubview:_detailTextView];
        [self addSubview:_viewWorkDetailBtn];
        [self addSubview:_likeBtn];
    
        [self bringSubviewToFront:_likeBtn];
        [self bringSubviewToFront:_viewWorkDetailBtn];
        
    }
    
    return self;
}

-(void)buttonDetailClicked:(id)sender{

    NSLog(@"Detail Clicked");
    [self.delegate viewWorkDetailBtnPressed:self];
}

-(void)buttonLikeClicked:(id)sender{
    
    NSLog(@"Like Clicked");
    [self.delegate likeBtnPressed:self];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
