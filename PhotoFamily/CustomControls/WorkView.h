//
//  WorkView.h
//  PhotoFamily
//
//  Created by EDIT Guest on 12/06/13.
//  Copyright (c) 2013 EDIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WorkViewDelegate;


@interface WorkView : UIView

@property(strong, nonatomic)UIImageView * workImgView;
@property(strong, nonatomic)UIImageView * authorImgView;
@property(strong, nonatomic)UITextView * detailTextView;
@property(strong, nonatomic)UIButton * likeBtn;
@property(strong, nonatomic)UIButton * viewWorkDetailBtn;
@property(assign, nonatomic)id<WorkViewDelegate>delegate;

- (id)initWithFrame:(CGRect)frame withWorkImage:(NSString *)workImage andAuthorImage:(NSString *)authorImage andText:(NSString *)detailText;
@end


@protocol WorkViewDelegate <NSObject>

-(void)likeBtnPressed:(WorkView *)workLiked;
-(void)viewWorkDetailBtnPressed:(WorkView *)imageToViewDetail;


@end