//
//  EDBaseResponse.m
//  EDITStub
//
//  Created by Tiago Janela on 20/05/13.
//  Copyright (c) 2013 Bliss Applications. All rights reserved.
//

#import "EDBaseResponse.h"

#define kEDBaseResponse_DictionaryKey_Status @"Status"
#define kEDBaseResponse_DictionaryKey_Message @"Message"
#define kEDBaseResponse_DictionaryKey_Data @"Data"

@implementation EDBaseResponse

+ (EDBaseResponse*) parseResponseFromDictionary:(NSDictionary*)dictionary
{
	EDBaseResponse *result = [[EDBaseResponse alloc] init];
	result.status = [dictionary objectForKey:kEDBaseResponse_DictionaryKey_Status];
	result.message = [dictionary objectForKey:kEDBaseResponse_DictionaryKey_Message];
	result.data = [dictionary objectForKey:kEDBaseResponse_DictionaryKey_Data];
	return result;
}

@end
