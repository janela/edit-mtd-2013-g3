//
//  EDArtworkManager.h
//  EDITStub
//
//  Created by Tiago Janela on 20/05/13.
//  Copyright (c) 2013 Bliss Applications. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EDArtworkManagerDataSource.h"

#define kEDArtworkManager_ErrorDomain @"EDArtworkManager"

typedef enum {
	kEDArtworkManager_Error_System = 0x100,
	kEDArtworkManager_Error_NoNetwork = 0x101,
	kEDArtworkManager_Error_ServerError = 0x102
}edArtworkManager_Error;

@interface EDArtworkManager : NSObject
{
	id<EDArtworkManagerDataSource> _dataSource;
}

- (void)getAllArtworksForUserId:(NSString*)userId withCompletionBlock:(void(^)(NSError* error, NSArray* artworks))completionBlock;

+ (EDArtworkManager*) sharedManager;

@end
