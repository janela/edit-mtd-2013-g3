//
//  EDArtwork.m
//  EDITStub
//
//  Created by Tiago Janela on 20/05/13.
//  Copyright (c) 2013 Bliss Applications. All rights reserved.
//

#import "EDArtwork.h"

#define kEDArtwork_DictionaryKey_Title @"Title"
#define kEDArtwork_DictionaryKey_ImageURL @"ImageURL"
#define kEDArtwork_DictionaryKey_AuthorURL @"AuthorURL"
#define kEDArtwork_DictionaryKey_Author @"Author"
#define kEDArtwork_DictionaryKey_Text @"Text"
#define kEDArtwork_DictionaryKey_Latitude @"Latitude"
#define kEDArtwork_DictionaryKey_Longitude @"Longitude"

@implementation EDArtwork

+ (NSArray*) parseObjectsFromDictionariesArray:(NSArray*)dictionariesArray{
	NSMutableArray *resultingArray = [NSMutableArray arrayWithCapacity:dictionariesArray.count];
	
	for (NSDictionary *dictionary in dictionariesArray) {
        [resultingArray addObject:[self parseObjectFromDictionary:dictionary]];
	}
	return resultingArray;
}

+ (EDArtwork*) parseObjectFromDictionary:(NSDictionary*)dictionary{
	EDArtwork *artwork = [[EDArtwork alloc]init];
	artwork.title = [dictionary objectForKey:kEDArtwork_DictionaryKey_Title];
	artwork.imageURL = [dictionary objectForKey:kEDArtwork_DictionaryKey_ImageURL];
	artwork.authorURL = [dictionary objectForKey:kEDArtwork_DictionaryKey_AuthorURL];
	artwork.author = [dictionary objectForKey:kEDArtwork_DictionaryKey_Author];
	artwork.text = [dictionary objectForKey:kEDArtwork_DictionaryKey_Text];
	artwork.latitude = [dictionary objectForKey:kEDArtwork_DictionaryKey_Latitude];
	artwork.longitude = [dictionary objectForKey:kEDArtwork_DictionaryKey_Longitude];
	return artwork;
}

@end
