//
//  EDArtworkManagerStubDataSource.m
//  EDITStub
//
//  Created by Tiago Janela on 20/05/13.
//  Copyright (c) 2013 Bliss Applications. All rights reserved.
//

#import "EDArtworkManagerStubDataSource.h"
#import "EDArtwork.h"
#import "EDBaseResponse.h"

#define kEDArtworkManager_StubArtworksPLIST @"ArtworksStub.plist"

@implementation EDArtworkManagerStubDataSource

- (void)artworkManager:(EDArtworkManager *)artworkManager getAllArtworksForUserId:(NSString *)userId withCompletionBlock:(void (^)(NSError *, EDBaseResponse *))completionBlock
{
	NSError* error = nil;
	
	NSString *finalPath = [[NSBundle mainBundle] pathForResource:kEDArtworkManager_StubArtworksPLIST ofType:nil];
	
	NSDictionary *artworksStub = [NSDictionary dictionaryWithContentsOfFile:finalPath];
	EDBaseResponse *realArtworks = [EDBaseResponse parseResponseFromDictionary:artworksStub];
	realArtworks.data = [EDArtwork parseObjectsFromDictionariesArray:realArtworks.data];
	completionBlock(error,realArtworks);
}

@end
