//
//  EDArtworkManager.m
//  EDITStub
//
//  Created by Tiago Janela on 20/05/13.
//  Copyright (c) 2013 Bliss Applications. All rights reserved.
//

#import "EDArtworkManager.h"
#import "EDArtworkManagerStubDataSource.h"
#import "EDArtworkManagerDynamicDataSource.h"
#import "EDBaseResponse.h"

#define kEDArtworkManager_UseStubDataSource 1



@implementation EDArtworkManager

- (void)getAllArtworksForUserId:(NSString*)userId withCompletionBlock:(void(^)(NSError* error, NSArray* artworks))completionBlock
{
	[NSThread MCSM_performBlockInBackground:^{
		[_dataSource artworkManager:self getAllArtworksForUserId:userId withCompletionBlock:^(NSError *error, EDBaseResponse *artworks) {
            [NSThread MCSM_performBlockOnMainThread:^{
                NSError *convertedError = nil;
                if(error)
                {
                    convertedError = [NSError errorWithDomain:kEDArtworkManager_ErrorDomain code:kEDArtworkManager_Error_System userInfo:nil];
                }
                else if(artworks.status != 0)
                {
                    convertedError = nil;
                }
                completionBlock(convertedError,artworks.data);
            }];
        }];
	}];
}

#pragma mark -
#pragma mark Object Lifecycle

- (id)init
{
	self = [super init];
	if (self) {
		if(kEDArtworkManager_UseStubDataSource){
			_dataSource = [[EDArtworkManagerStubDataSource alloc] init];
		}else {
			_dataSource = [[EDArtworkManagerDynamicDataSource alloc] init];
			//NSAssert(false, @"No known implementation for EDArtworkManagerDataSource.");
		}
	}
	return self;
}

static EDArtworkManager* _sharedManager;

+ (EDArtworkManager *)sharedManager{
	if(!_sharedManager){
		_sharedManager = [[EDArtworkManager alloc] init];
	}
	return _sharedManager;
}

@end















