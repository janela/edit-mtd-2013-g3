//
//  EDArtworkManagerDataSource.h
//  EDITStub
//
//  Created by Tiago Janela on 20/05/13.
//  Copyright (c) 2013 Bliss Applications. All rights reserved.
//

#import <Foundation/Foundation.h>
@class EDBaseResponse;
@class EDArtworkManager;

@protocol EDArtworkManagerDataSource <NSObject>

- (void) artworkManager:(EDArtworkManager*)artworkManager getAllArtworksForUserId:(NSString*)userId withCompletionBlock:(void(^)(NSError* error, EDBaseResponse* response))completionBlock;

@end
