//
//  EDArtworkManagerDynamicDataSource.m
//  EDITStub
//
//  Created by Tiago Janela on 20/05/13.
//  Copyright (c) 2013 Bliss Applications. All rights reserved.
//

#import "EDArtworkManagerDynamicDataSource.h"
#import "EDArtwork.h"
#import "EDBaseResponse.h"

#define kEDArworkManager_DynamicDataSource_EndpointURL @"https://gist.github.com/tjanela/5615298/raw/1fc95c870641281d74bf87abacfa0e72b5b2405a/ArtworksSimulation"

@implementation EDArtworkManagerDynamicDataSource

- (void)artworkManager:(EDArtworkManager *)artworkManager getAllArtworksForUserId:(NSString *)userId withCompletionBlock:(void (^)(NSError *, EDBaseResponse *))completionBlock
{
	NSURL *url = [NSURL URLWithString:kEDArworkManager_DynamicDataSource_EndpointURL];
	NSURLRequest *request = [NSURLRequest requestWithURL:url];
	NSURLResponse *response = nil;
	NSError *error = nil;
	NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
	NSDictionary *unparsedArtworks = [[CJSONDeserializer deserializer] deserializeAsDictionary:responseData error:&error];
	
	EDBaseResponse *realArtworks = [EDBaseResponse parseResponseFromDictionary:unparsedArtworks];
	realArtworks.data = [EDArtwork parseObjectsFromDictionariesArray:realArtworks.data];
	completionBlock(error,realArtworks);
}

@end
