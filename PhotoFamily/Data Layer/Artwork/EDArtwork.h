//
//  EDArtwork.h
//  EDITStub
//
//  Created by Tiago Janela on 20/05/13.
//  Copyright (c) 2013 Bliss Applications. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EDArtwork : NSObject

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *imageURL;
@property (strong, nonatomic) NSString *authorURL;
@property (strong, nonatomic) NSString *author;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *longitude;

+ (NSArray*) parseObjectsFromDictionariesArray:(NSArray*)dictionariesArray;
+ (EDArtwork*) parseObjectFromDictionary:(NSDictionary*)dictionary;

@end
