//
//  DetalheViewController.m
//  PhotoFamily
//
//  Created by EDIT Guest on 29/04/13.
//  Copyright (c) 2013 EDIT. All rights reserved.
//

#import "PFDetailVC.h"

@interface PFDetailVC ()

@end

@implementation PFDetailVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{

    self.imgViewDetail.image = _theWorkView.workImgView.image;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
