//
//  AppDelegate.h
//  PhotoFamily
//
//  Created by EDIT Guest on 29/04/13.
//  Copyright (c) 2013 EDIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
